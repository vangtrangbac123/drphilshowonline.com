<?php

class Admin_BannerController extends My_Controller_Form {

    public $_form = 'Banner';
    public function getList(){
         $sql = "SELECT
                        b.banner_id,
                        b.banner_url,
                        b.banner_image,
                        b.banner_title,
                        b.is_active,
                        (CASE
                        	position_id
                            WHEN 1 THEN 'Banner Slide'
                            WHEN 2 THEN 'Banner SideBar'
                            END
                        	) position
                FROM banner_v4 b
                ORDER BY b.date_add DESC, b.is_active DESC, b.banner_id, b.position_id";
        return $this->model->Banner->getRows($sql);
    }

    public function getDetail($id) {
        $sql = "SELECT
                        b.banner_id,
                        b.banner_url,
                        b.banner_image path,
                        b.banner_title,
                        b.is_active,
                        b.position_id
                FROM banner_v4 b
                WHERE b.banner_id = :banner_id";
        return $this->model->Artist->getRow($sql, array('banner_id' => $id));
    }

    public function onSaveBefore($data, $post) {
       return $data;
    }

}