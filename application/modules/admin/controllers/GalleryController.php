<?php

class Admin_GalleryController extends My_Controller_Form {

    public $_form = 'Gallery';

    public function getDetail($id) {
        $gallery = $this->model->Gallery->get($id);
        $sql = 'SELECT  i.*
                FROM gallery_image gi
                JOIN images i ON i.image_id = gi.image_id
                WHERE gi.gallery_id = :gallery_id AND gi.is_active = 1 AND gi.type_id =1';
        $gallery->images = $this->model->Gallery->getRows($sql, array('gallery_id' => $id));
        return $gallery;
    }
}

