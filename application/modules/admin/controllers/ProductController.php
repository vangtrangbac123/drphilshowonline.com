<?php

class Admin_ProductController extends My_Controller_Form {

    public $_form = 'Product';

    public function getList() {

        $is_active = (int)$this->_getParam('is_active', -1);

        $bin =array();
        $where ='';

        if($is_active != -1 ){
            $where .=' AND f.is_active = :is_active';
            $bin['is_active'] = $is_active;
        }


        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    p.product_id, p.product_name, p.is_active, p.number, i.size1,
                    ct.category_name type
                FROM product p
                LEFT JOIN product_image pi ON pi.product_id = p.product_id AND pi.type_id = 1 AND pi.is_active = 1
                LEFT JOIN images i ON i.image_id = pi.image_id
                JOIN categories ct ON ct.category_id = p.category_id
                WHERE 1 = 1 $where
                ORDER BY  p.number , p.date_add DESC";
        $data = $this->getListAutoPaging($sql,$bin);

        $data['sort'] = new stdClass();
        if($is_active != -1 ) $data['sort']->is_active = $is_active;
        return $data;
    }

    public function getDetail($id) {
        $product = $this->model->Product->get($id);
        $sql = 'SELECT pi.type_id, i.*
                FROM product_image pi
                JOIN images i ON i.image_id = pi.image_id
                WHERE pi.product_id = :product_id AND pi.is_active = 1
                ORDER BY pi.type_id';
        $images = $this->model->Product->getRows($sql, array('product_id' => $id));
        $group = array();
        foreach ($images as $img) {
            $group[$img->type_id][] = $img;
            $product->{sprintf('image_%d_%d', $img->type_id, 1)} = $img->size1;
            $product->{sprintf('image_%d_%d', $img->type_id, 2)} = $img->size2;
            $product->{sprintf('image_%d_%d', $img->type_id, 3)} = $img->size3;


        }
        $product->images = $group;

        return $product;
    }
    public function galleryAction() {
        $id = $this->_getParam('id', 0);
        $r = $this->getDetail($id);
        $this->view->data = $this->getDetail($id);
    }


    public function onSaveBefore($data, $post) {
        if (empty($data['product_slug'])) {
            $data['product_slug'] = Utility_Unicode::get_str_replace($data['product_name']);
        }

   /*     if ($data['product_id']) {
           $data['url_short'] = Utility_Unicode::convertLink(0,$data['product_id'],$data['product_slug']);
        }*/
        if (isset($data['category_id']) ) {
            $data['category_id'] = intval($data['category_id']);
            $category = $this->model->Category->get(array('category_id' => $data['category_id']));
            $data['url_short'] = '/shows/'.$category->category_slug.'/'. $data['product_slug'];
        }
        //echo json_encode($data);die;
        return $data;
    }

    public function onSaveAfter($id, $data) {
        $this->updateTags($id, $data['tags']);
        return $data;
    }

    private function updateTags($pId, $tags) {
        $pId = intval($pId);
        if ($pId == 0) return;

        $this->model->Tag->delProductTag($pId);

        $tags = trim($tags);
        if ($tags == '') return;

        $tags = explode(',', $tags);
        if (count($tags) == 0) return;

        $sep = '';
        $sql = 'INSERT IGNORE INTO product_tag (`product_id`, `tag_id`) VALUES ';
        foreach ($tags as $name) {
            $row = $this->model->Tag->checkTag(trim($name));
            if ($row) {
                $tagId = (int)$row->tag_id;
                $sql .= $sep . "($pId, $tagId)";
                $sep = ', ';
            }
        }

        return $this->model->Tag->_excute($sql);
    }


}

