            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">

                <div class="sidebar-header">
                     <div class="sidebar-title">
                        <?php echo $this->user->fullname?> | <a href="/auth/logout/" title="Sign Out">Sign Out</a>
                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>

                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li data-controller="dashboard">
                                    <a href="/dashboard/">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard</span>
                                    </a>
                                </li>
                                <li class="nav-parent" data-controller="meta">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>Meta</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/meta/detail/">Add Meta </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/meta/">Meta</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-parent" data-controller="menu">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>Menu</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/menu/detail/">Add Menu </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/menu/">Menu</a>
                                        </li>
                                    </ul>
                                </li> 

                                <li class="nav-parent" data-controller="news">
                                    <a>
                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>News</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li data-action="detail">
                                            <a href="/admin/news/detail/">Add News</a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/news/">List Post</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-parent" data-controller="category">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>Category</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/category/detail/">Add Category </a>
                                        </li>
                                        <li data-action="web">
                                            <a href="/admin/category/">Category</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-parent" data-controller="product">
                                    <a>
                                        <i class="fa fa-film" aria-hidden="true"></i>
                                        <span>Product</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li data-action="detail">
                                            <a href="/admin/product/detail/">Add Product</a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/product/">List Product</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>

                        <hr class="separator">

                       


                        <hr class="separator">

                        <div class="sidebar-widget widget-tasks">
                            <div class="widget-header">
                                <h6>Others</h6>
                            </div>
                            <div class="widget-content">
                                <ul class="list-unstyled m-none">
                                    <li><a href="#">Clear cache</a></li>
                                    <li><a href="#">Old version</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>

            </aside>
            <!-- end: sidebar -->