<?php

$fields = array();

$sort   = array();

$sort['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => 'sort',
    'list' => Admin_Model_Form::getListActive(),
    'key'  => 'is_active'
);

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);


$fields['is_hot'] = array(
    'label' => 'Hot?',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);


$fields['category_id'] = array(
    'label' => 'Category',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListCategory(),
);


$fields['product_name'] = array(
    'label' => 'Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['link_youtube'] = array(
    'label' => 'Youtube Link',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);
$fields['link_facebook'] = array(
    'label' => 'FAcebook Link',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['number'] = array(
    'label' => 'Number',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['url_short'] = array(
    'label' => 'Short Link',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOXREAD
);

$fields['publish_date'] = array(
    'label' => 'Publish date',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_DATEBOX
);


$fields['tags'] = array(
    'label' => 'Tags',
    'data'  => Admin_Model_Form::DATA_STRING,
    'type'  => Admin_Model_Form::TYPE_TAGS_INPUT,
);


$fields['@image_1'] = array(
    'label' => 'Poster<br/>600x440<br/>300x220<br/>100x75',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_IMAGE,
    'image' => array(
         array(
            'key'    => 'image_1_3',
            'width'  => 600,
            'height' => 420,
            'type'   => 1,
            'size'   => 3,
        ),
        array(
            'key'    => 'image_1_2',
            'width'  => 300,
            'height' => 210,
            'type'   => 1,
            'size'   => 2,
        ),
        array(
            'key'    => 'image_1_1',
            'width'  => 70,
            'height' => 50,
            'type'   => 1,
            'size'   => 1,
        )
    )
);
$fields['description'] = array(
    'label' => 'Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);


$fields['content'] = array(
    'label' => 'Content',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTEDITOR2
);

$fields['meta_title'] = array(
    'label' => 'Meta title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['meta_description'] = array(
    'label' => 'Meta Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['meta_keyword'] = array(
    'label' => 'Meta Keyword',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);



$listview = array(
    'part' => 'product',
    'colums' => array('#', 'Image', 'Name','Number' ,'Gallery','Active')
);

$list = array();
$list['model'] = 'Product';
$list['form']  = 'Product';
$list['table'] = 'product';
$list['primary'] = 'product_id';
$list['fields'] = $fields;
$list['listview'] = $listview;
$list['sort'] = $sort;

return $list;