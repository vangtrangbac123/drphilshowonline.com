<?php

class Admin_Model_Film extends My_Model_Abstract {

    protected $_name    = 'films';
    protected $_primary = 'film_id';

    public function getListFilmTmpFilm($groupId, $filmId) {
        $sql = 'SELECT t.tmp_film_id, t.sMovie, t.sUntouchedMovieText, t.date_update, f.date_add last_mapping,
                    IF (f.film_id = :film_id, 1, 0) is_checked,t.is_voice
                FROM tmp_film t
                LEFT JOIN film_tmp_film f ON t.tmp_film_id = f.tmp_film_id
                WHERE t.p_cinema_id = :p_cinema_id AND t.is_active = 1
                GROUP BY t.tmp_film_id
                ORDER BY is_checked DESC, t.sUntouchedMovieText ASC';
        return $this->_db->fetchAll($sql, array('p_cinema_id' => $groupId, 'film_id' => (int)$filmId));
    }
}