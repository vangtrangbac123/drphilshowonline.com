<?php

class Admin_Model_FilmArtist extends My_Model_Abstract {

    protected $_name    = 'film_artist';
    protected $_primary = 'film_id';

}