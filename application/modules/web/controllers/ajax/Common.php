<?php

class Android_Ajax_Common{


    public $_controller;
	protected $_object;
    private $api;

	function __construct($obj = null, $controller = null) {
		$this->_object     = $obj;
		$this->_controller = $controller;
        $this->api = My_Api_Api::getInstance();
	}
    public function getLocation($params){
       if (!isset($params['location_id'])) return false;
        try {
            $locationModel = new Admin_Model_Location();
            return $locationModel->getListDistrict(intval($params['id']));
        } catch (Exception $e) {
        }

        return false;
    }
    public function check($params){
         if (!isset($params['user_id'])) return false;
         if (!isset($params['branch_id'])) return false;
         if (!isset($params['bill_id'])) return false;
         if (!isset($params['brand_id'])) return false;
         if ($params["bill_id"]=="") return "Vui lòng nhập số hóa đơn";

        try {
             $result= $this->api->post('User.loyalty.1.web', array(
                'user_id'   =>$params['user_id'],
                'branch_id' =>$params['branch_id'],
                'bill_id'   =>$params['bill_id'],
                'brand_id'  =>$params['brand_id']
             ));
             return $result["result"];
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        return false;
    }
    
}
