<?php

class Web_NewsController extends My_Controller_Web {

    public function init() {
    }

    public function indexAction() {
    	$params = $this->getRequest()->getParams();

        $page     = isset($params['page']) ? abs(intval($params['page'])) : 1;
        $count    = 10;
        $offset   = $count * ($page - 1);

        $result = $this->model->News->getList($offset, $count);

        $this->view->list_news   =  $result['rows'];

        $this->view->paginator = Utility_Paginator::create('/tin-tuc/?page=%d', $page, $count, $result['total']);

        }
    public function introAction(){
        	$this->view->news   = $this->model->News->getNewsIntro();
    }

    public function contactAction(){
    	$this->_helper->viewRenderer('intro');
    	$this->view->news   = $this->model->News->getNewsContact();

    }
    public function detailAction(){
    	$params = $this->getRequest()->getParams();

        if (!isset($params['nId']) || !isset($params['nName'])) {
            $this->_redirect('/');
        }
        $news = $this->model->News->getDetail($params['nId']);
        $this->view->news = $news;
        $this->view->list_relate = $this->model->News->getListRelate((array)$news);
    }

}
