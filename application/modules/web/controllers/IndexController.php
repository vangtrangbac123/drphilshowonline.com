<?php

class Web_IndexController extends My_Controller_Web {

    public function init() {
    }

    public function indexAction() {
    	$sql = "SELECT banner_image FROM banner_v4 WHERE position_id = 1 AND is_active = 1 ORDER BY date_add DESC";
		$this->view->banner_slide = $this->model->Banner->getRows($sql);
		$sql = "SELECT news_id,news_title,news_url,news_description FROM news WHERE type_id = 3 AND is_active = 1 ORDER BY news_id DESC LIMIT 3";
		$this->view->news = $this->model->News->getRows($sql);
    }
}
