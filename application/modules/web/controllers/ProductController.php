<?php

class Web_ProductController extends My_Controller_Web {

    public function init() {
    }
    public function indexAction(){

    }

    public function detailAction() {
    	$params = $this->getRequest()->getParams();

        if (!isset($params['pId']) || !isset($params['pName'])) {
            $this->_redirect('/');
        }
        $product = $this->model->Product->getDetail($params['pId']);
        $this->view->product = $product;
        $this->view->list_relate = $this->model->Product->getListRelate((array)$product);
        $this->view->list_gallery = $this->model->Product->getListGallery($params['pId']);
    }
}
