<?php

class Drphil_IndexController extends My_Controller_Web {

    public function init() {
    }

    public function indexAction() {
    	$this->view->list_slide = $this->model->Product->getListHot();
    	$this->view->list_category = $this->model->Product->getListByCategory();
    	$this->view->list_lasted = $this->model->Product->getListLasted();
    }

    public function detailAction(){
    	
    }

    public function categoryAction(){
        
    }

}
