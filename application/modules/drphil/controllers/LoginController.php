<?php

class Drphil_LoginController extends My_Controller_Web {

    public function init() {
    }

    public function indexAction(){
        if ($this->_request->isPost()) {

            $post = $this->_request->getPost();
            $post = array_map('trim', $post);

            if (!empty($post['username']) && !empty($post['password'])) {

                // check user exists
                $user = $this->model->Visitor->get(array('username' => $post['username']));

                if ($user) {

                    if (!empty($user->password) && $user->password == md5($post['password'])) {

                       $result = true;

                    } else {

                       $result = false;
                    }

                    if ($result) {

                        $user->fullname = trim($user->fullname);
                        $user->user_id  = intval($user->user_id);
                        $user->level    = $user->level;

                        if (empty($user->fullname)) {
                            $user->fullname = $user->username;
                        }


                        $this->auth->getStorage()->write($user);
                        $this->_redirect('/');
                    }
                }
            }

            $this->view->error    = 'Wrong username or password.';
            $this->view->username = $post['username'];
            $this->view->password = $post['password'];
        }

    }
    public function registerAction(){
        $this->view->error = "";
        if ($this->_request->isPost()) {

            $post = $this->_request->getPost();

            $post = array_map('trim', $post);

            if(empty($post['username']) || empty($post['password'])){
                $this->view->error    = 'Please Insert Full Imformation';
            }

            if (!empty($post['username']) && !empty($post['password'])) {

                // check user exists
                $user = $this->model->Visitor->get(array('username' => $post['username']));

                if ($user) {
                    $this->view->error    = 'Username Exits in System. Please Register With other UserName ';
                    //$this->_redirect('/');
                    
                }else{

                     $data = array(
                        'fullname' => $post['fullname'],
                        'username' => $post['username'],
                        'password' => md5($post['password']),
                        'email'    => $post['email'],
                        'facebook' => $post['facebook'],
                        'level'    => 1,
                        'status'   => 1,
                        'date_add' => date("Y-m-d H:i:s"),
                    );

                     $r = $this->model->Visitor->save($data);
                     $this->view->registersuccess = true;
                     $user = $this->model->Visitor->get(array('username' => $post['username']));
                     if($user){
                        $user->fullname = trim($user->fullname);
                        $user->user_id  = intval($user->user_id);
                        $user->level    = $user->level;

                        if (empty($user->fullname)) {
                            $user->fullname = $user->username;
                        }


                        $this->auth->getStorage()->write($user);

                     }

                }
            }
            $this->view->username = $post['username'];

        }
        


    }


     public function logoutAction() {

        if ($this->auth->hasIdentity()) {
            $this->auth->clearIdentity();
        }

        $this->_redirect('/');
    }
}
