<?php

class Drphil_PaymentController extends My_Controller_Web {

    public function init() {
    }

    public function indexAction(){
        

    }

     public function successAction() {

        //$params = $this->getRequest()->getParams();
        //$page     = isset($params['page']) ? abs(intval($params['page'])) : 1;
        $user = $this->model->Visitor->get(array('user_id' => $this->user->user_id));

        if($user->tmp_status == 1){
        	$price = $user->money + intval($user->tmp_price);
	        $data = array(
	        	"user_id" => $user->user_id,
	        	"money"	  => $price,
	        	"tmp_status" => 0,
	        	"tmp_price"  => 0,
	        	);
	        $r = $this->model->Visitor->save($data);
            //Trnsaction
            $data2 = array(
                    "user_id"=>  $user->user_id,
                    "price"  =>  $user->tmp_price,
                    "status" => 1,
                    "date_add" => date("Y-m-d H:i:s"),
                    );
            $r = $this->model->Transaction->save($data2);

	        $this->view->price = $user->tmp_price;
        	$this->view->price_update = $price;
        }
        
        

    }
    public function cancelAction(){
    	$data = array(
	        	"user_id" => $this->user->user_id,
	        	"tmp_status" => 0,
	        	"tmp_price"  => 0,
	        	);
	    $r = $this->model->Visitor->save($data);
    }
    public function tempAction(){
    	if ($this->_request->isPost()) {

            $post = $this->_request->getPost();
            $post = array_map('trim', $post);
             if (!empty($post['user_id']) && !empty($post['price'])) {
             	 $data = array(
		        	"user_id" 	  => $post['user_id'],
		        	"tmp_price"	  => $post['price'],
		        	"tmp_status"  => 1
		        	);
		        $r = $this->model->Visitor->save($data);
                //Trnsaction
                $data2 = array(
                    "user_id"=> $post['user_id'],
                    "price"  => $post['price'],
                    "status" => 0,
                    "date_add" => date("Y-m-d H:i:s"),
                    );
                $r = $this->model->Transaction->save($data2);

             	if($r) echo json_encode(true);die;
             }
             echo json_encode(false);die;
            
         }
    }
}
