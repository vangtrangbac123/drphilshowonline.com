<?php

class Drphil_ContactController extends My_Controller_Web {

    public function init() {
    }

    public function indexAction(){
        
        $this->view->error = "";
        if ($this->_request->isPost()) {

            $post = $this->_request->getPost();

            $post = array_map('trim', $post);

            if(empty($post['email']) || empty($post['message'])){
                $this->view->error    = 'Please Insert Full Imformation';
            }

            if (!empty($post['email']) && !empty($post['message'])) {

                // check user exists
               
                $data = array(
                    'email'     => $post['email'],
                    'subject'   => $post['subject'],
                    'message'   => $post['message'],
                    "user_id"   =>"",
                    "username" =>"",
                    "fullname"  =>"",
                    "date_add"  => date("Y-m-d H:i:s"),
                    );
                if(!empty($this->user->user_id)){

                    $data['user_id'] = $this->user->user_id;
                    $data['fullname'] = $this->user->fullname;
                    $data['username'] = $this->user->username;
                   
                }
                 $r = $this->model->Contact->save($data);
                 $this->view->registersuccess = true;
            }
        }
    }

    
}
