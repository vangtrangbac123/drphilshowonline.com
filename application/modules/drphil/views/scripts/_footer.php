<footer>
	<div id="inner-footer" class="row clearfix widget">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<?php echo $this->render('/part/footer/col1.phtml')?>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<?php echo $this->render('/part/footer/col2.phtml');?>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<?php echo $this->render('/part/footer/col3.phtml');?>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<?php echo $this->render('/part/footer/col4.phtml');?>
		</div>
	</div>
</footer>