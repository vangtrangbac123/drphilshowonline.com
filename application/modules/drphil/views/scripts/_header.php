<?php 
  /*if(isset( $this->user->fullname)){
        $this->user = $this->model->Visitor->get(array('user_id' => $this->user->user_id));

        if (empty($this->user->fullname)) {
            $this->user->fullname = $this->user->username;
        }

    } */
?>
<header>
  <div class="visible-lg visible-md">
      <div class="inner-header">
          <div class="logo">
              <div class="row">
                <div class="col-md-6 col-lg-6 columns">
                  <h1>
                    <a href="/" title="Dr phil Show Online">
                      <img src="/layouts/drphil/img/header.jpg" title="Dr phil Show Online" height="100px" width="350px">
                    </a>
                  </h1>
                </div>
                <div class="col-md-6 col-lg-6 columns">
                
                  <div style="float:right;margin-top: 50px;font-size: 20px">
                    <?php if(isset( $this->user->fullname)){?>
                    <div>
                    <?php echo $this->user->fullname?> | <a href="/login/logout/" title="Sign Out">Sign Out</a>
                    </div>
                    <div><a class="w3-button" href="/myaccount" style="color: #FFFFFF;background-color: #4CAF50;">My Account</a> <a class="w3-button" href="#" style="color: #FFFFFF;background-color: #4CAF50;"><?php echo $this->user->money?>$</a></div>

                     <?php }else{?>

                     <div  class="col-md-6 col-lg-6 columns"><img src="/layouts/drphil/img/avatar.png" width="50px" height="50px" style="float:right"></div>
                     <div  class="col-md-6 col-lg-6 columns">
                     <a href="/login" title="Login" class="login">Login Now</a>
                     <a href="/login/register" title="Register" class="login">Register</a>
                     </div>

                    <?php } ?>
                  </div>
                 
                </div>
              </div>
          </div>
          <div class="menu-bar">
              <div class="row">
                <div class="col-md-12 col-lg-12" style="background-color:rgba(244, 244, 244, 0.56);">
                    <ul class="nav navbar-nav">
                    <?php $menu = My_Registry::getInstance()->getListMenu();
                        foreach ($menu as $key => $item):
                    ?>
                     <li class="<?php echo ((strpos($_SERVER['REQUEST_URI'], $item->url) !== false && $item->url != "/" && $_SERVER['REQUEST_URI'] != "/")||($_SERVER['REQUEST_URI'] == $item->url))?'active':''?>"><a href="<?php echo $item->url?>" title="<?php echo $item->menu_name?>"><?php echo $item->menu_name?></a></li>
                   <?php endforeach;?>
                    </ul>
                    <div class="search">
                       <input type="textbox" class="s-text" placeholder="Search video ..." >
                       <input type="submit" class="s-submit"  value="">
                    </div>

                </div>
              </div>  
            </div>
      </div>
  </div>
  <div class="visible-sm  visible-xs navbar navbar-inverse navbar-fixed-top" role="navigation" id="slide-nav">
    <div class="container">
     <div class="navbar-header">
      <a class="navbar-toggle"> 
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
       </a>
      <a class="navbar-brand" href="#">DR PHIL TODAY</a>
     </div>
     <div id="slidemenu">
      <ul class="nav navbar-nav">
          <?php $menu = My_Registry::getInstance()->getListMenu();
                foreach ($menu as $key => $item):
            ?>
             <li class="<?php echo ((strpos($_SERVER['REQUEST_URI'], $item->url) !== false && $item->url != "/" && $_SERVER['REQUEST_URI'] != "/")||($_SERVER['REQUEST_URI'] == $item->url))?'active':''?>"><a href="<?php echo $item->url?>" title="<?php echo $item->menu_name?>"><?php echo $item->menu_name?></a></li>
           <?php endforeach;?>
      </ul>
     </div>
    </div>
   </div>
</header>
<style type="text/css">
  .login{
    padding: 1px;color:white;background: blue;
  }
  .w3-button {
    border: none;
    display: inline-block;
    outline: 0;
    padding: 4px 8px;
    vertical-align: middle;
    overflow: hidden;
    text-decoration: none;
    color: inherit;
    background-color: inherit;
    text-align: center;
    cursor: pointer;
    white-space: nowrap;
}
</style>