<?php

class Drphil_Model_Product extends My_Model_Abstract {

    protected $_name    = 'product';
    protected $_primary = 'product_id';

    public function getListHot() {
    	$sql = "SELECT
	    			product_id,
	    			product_name,
	    			link_youtube,
                    link_facebook,
	    			description,
	    			url_short,
                    publish_date
    			FROM product
    			WHERE is_active =1 AND is_hot = 1
    			ORDER BY publish_date DESC, date_add DESC
    			LIMIT 5
    			";
    	return $this->_db->fetchAll($sql);
    }

    public function getListByCategory() {
    	$sql = "SELECT
	    			p.product_id,
	    			p.product_name,
	    			p.link_youtube,
	    			p.description,
	    			p.url_short,
	    			c.category_name,
                    p.publish_date,
                    c.url_short category_url,
	    			i.size2
    			FROM product p 
    			JOIN categories c ON c.category_id = p.category_id
    			LEFT JOIN product_image pm ON pm.product_id = p.product_id AND pm.is_active =1 AND pm.type_id =1
    			LEFT JOIN images i ON i.image_id = pm.image_id 
    			WHERE p.is_active = 1 AND p.is_hot = 1
    			GROUP BY p.product_id
    			ORDER BY p.publish_date DESC, p.date_add DESC
    			LIMIT 10
    			";
    	return $this->_db->fetchAll($sql);
    }

    public function getListLasted(){
        $sql = "SELECT
                    p.product_id,
                    p.product_name,
                    p.link_youtube,
                    p.description,
                    p.url_short,
                    p.publish_date,
                    i.size2
                FROM product p
                LEFT JOIN product_image pm ON pm.product_id = p.product_id AND pm.is_active =1 AND pm.type_id =1
                LEFT JOIN images i ON i.image_id = pm.image_id
                WHERE p.is_active = 1
                GROUP BY p.product_id
                ORDER BY p.publish_date DESC, p.date_add DESC
                LIMIT 6
                ";
        return $this->_db->fetchAll($sql);
    }

    public function getListAll(){
        $sql = "SELECT
                    p.product_id,
                    p.product_name,
                    p.link_youtube,
                    p.description,
                    p.url_short,
                    p.publish_date,
                    i.size2
                FROM product p
                LEFT JOIN product_image pm ON pm.product_id = p.product_id AND pm.is_active =1 AND pm.type_id =1
                LEFT JOIN images i ON i.image_id = pm.image_id
                WHERE p.is_active = 1
                GROUP BY p.product_id
                ORDER BY p.publish_date DESC, p.date_add DESC
                ";
        return $this->_db->fetchAll($sql);
    }


    public function getListCategory($category_slug){
        $sql = "SELECT
                    p.product_id,
                    p.product_name,
                    p.link_youtube,
                    p.description,
                    p.url_short,
                    p.publish_date,
                    c.category_name,
                    i.size2
                FROM product p
                LEFT JOIN product_image pm ON pm.product_id = p.product_id AND pm.is_active =1 AND pm.type_id =1
                LEFT JOIN images i ON i.image_id = pm.image_id
                JOIN categories c ON c.category_id = p.category_id
                WHERE p.is_active = 1 AND c.category_slug = :category_slug
                GROUP BY p.product_id
                ORDER BY p.publish_date DESC, p.date_add DESC
                ";
        return $this->_db->fetchAll($sql,array('category_slug' => $category_slug));
    }

    
    public function getDetail($product_slug){
        $sql = "SELECT
                    p.product_id,
                    p.product_name,
                    p.description,
                    p.link_youtube,
                    p.link_facebook,
                    p.url_short,
                    p.content,
                    p.publish_date,
                    p.category_id,
                    p.meta_title,
                    p.meta_description,
                    p.meta_keyword
                FROM product p
                WHERE  p.product_slug = :product_slug
                ";
        $item = $this->_db->fetchRow($sql,array('product_slug' => $product_slug));
        if(count($item) > 0){
             $sql = "SELECT
                        p.product_id,
                        p.product_name,
                        p.link_youtube,
                        p.description,
                        p.url_short,
                        p.publish_date,
                        i.size1
                    FROM product p
                    LEFT JOIN product_image pm ON pm.product_id = p.product_id AND pm.is_active =1 AND pm.type_id =1
                    LEFT JOIN images i ON i.image_id = pm.image_id
                    WHERE p.is_active = 1 AND p.category_id = :category_id AND p.product_id <> :product_id
                    GROUP BY p.product_id
                    ORDER BY p.publish_date DESC, p.date_add DESC
                    LIMIT 6
                    ";
            $item->list_related = $this->_db->fetchAll($sql,array('category_id' => $item->category_id, 'product_id' => $item->product_id));
            $sql = "SELECT
                        p.product_id,
                        p.product_name,
                        p.link_youtube,
                        p.description,
                        p.url_short,
                        p.publish_date,
                        i.size1
                    FROM product p
                    LEFT JOIN product_image pm ON pm.product_id = p.product_id AND pm.is_active =1 AND pm.type_id =1
                    LEFT JOIN images i ON i.image_id = pm.image_id
                    WHERE p.is_active = 1 AND p.category_id = :category_id AND p.product_id < :product_id
                    GROUP BY p.product_id
                    LIMIT 1
                    ";
            $item->item_previous = $this->_db->fetchRow($sql,array('category_id' => $item->category_id, 'product_id' => $item->product_id));

            $sql = "SELECT
                        p.product_id,
                        p.product_name,
                        p.url_short,
                        p.publish_date,
                        i.size1
                    FROM product p
                    LEFT JOIN product_image pm ON pm.product_id = p.product_id AND pm.is_active =1 AND pm.type_id =1
                    LEFT JOIN images i ON i.image_id = pm.image_id
                    WHERE p.is_active = 1 AND p.category_id = :category_id AND p.product_id > :product_id
                    GROUP BY p.product_id
                    LIMIT 1
                    ";
            $item->item_next = $this->_db->fetchRow($sql,array('category_id' => $item->category_id, 'product_id' => $item->product_id));
        }
        return $item;

    }
}