<?php
$file = 'upload.txt';

$target_dir = "/";
$target_file = time()."_".basename($_FILES["file_upload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    if (move_uploaded_file($_FILES["file_upload"]["tmp_name"], $target_file)) {
        echo "The file <span style='font-size:20px;color:red'>". $target_file. "</span> has been uploaded.<br>";

        $oldContents = file_get_contents($file);
        $fr = fopen($file, 'w');
        $newmsg = $target_file."\n".$oldContents;
        fwrite($fr, $newmsg);
        fclose($fr);

        $lines = file($file);

        // Loop through our array, show HTML source as HTML source; and line numbers too.
        foreach ($lines as $line_num => $line) {
            echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
            if($line_num > 10) break;
        }

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}else{
    $lines = file($file);

    // Loop through our array, show HTML source as HTML source; and line numbers too.
    foreach ($lines as $line_num => $line) {
        echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
        if($line_num > 10) break;
    }
}
?>