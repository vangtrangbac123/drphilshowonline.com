var Core123F = {
    // Dev
    ENV: 'production',
    DEBUG: false,
    domain: document.host,
    controllerName: '',
    actionName: '',
    callStack: [],
    data: {
    },
    // init
    init: function() {
        $.post(Core123F.ajax.baseUrl + '?method=Account.login', {}, function(result) {
            if (result && typeof result.account == 'object') {
                Core123F.account = result.account;
                Core123F.updateLoginStatus();
            }
        });


    },

    // call method by module
    exec: function(controller, action) {
        var action = (action === undefined) ? 'init' : action;
        var module = this['modules'];
        if (controller !== '' && module[controller]
            && typeof module[controller][action] == 'function') {
            module[controller][action]();
        }
    },


    ajax: {
        baseUrl: '/default/ajax/',
        get: function(api, params, callback) {
            if (typeof params == 'function') {
                callback = params;
                params = null;
            }
            params = $.extend({'method': api}, params);
            return $.getJSON(Core123F.ajax.baseUrl, params, function(results) {
                if (results != false && results != null) {
                    callback.call(Core123F.ajax, results);
                } else {
                    callback.call(Core123F.ajax, null);
                }
            });
        },
        post: function(api, params, callback) {
            return $.post(Core123F.ajax.baseUrl + '?method=' + api, params, callback);
        }
    },

    tracking: {
        ga: {
            event: function(category, action, label, value) {
              if (Core123F.ENV == 'local') {
                console.log('send','event',category, action,label,value);
              }
              if (typeof ga !== 'undefined') {
                ga('send','event',category, action,label,value);
              }

            }
        }
    },

    // DOM ready
    ready: function() {
        Core123F.init();
        this.controllerName = $('body').data('controller');
        this.actionName     = $('body').data('action');
        this.exec(this.controllerName);
        this.exec(this.controllerName, this.actionName);

        // call stack
        if (this.callStack[this.controllerName]
            && this.callStack[this.controllerName][this.actionName]
            && this.callStack[this.controllerName][this.actionName].length > 0) {
            for (var i = 0, f; f = this.callStack[this.controllerName][this.actionName][i]; i++) {
                f.call(this);
            };
        }
    },

    // function
    cookie: function(key, value, option) {
        if (value === undefined && option === undefined) {
            return $.cookie(key);
        }
        var defaults = {path: '/', domain: this.domain, expires: 365};
        var options = $.extend(defaults, option);
        return $.cookie(key, value, options);
    },

    // extend
    extend: function(func, controller, action) {
        controller = controller || 'index';
        action = action || 'index';
        if (!this.callStack[controller]) {
            this.callStack[controller] = {};
        }
        if (!this.callStack[controller][action]) {
            this.callStack[controller][action] = [];
        }
        this.callStack[controller][action].push(func);
    },
    plugins: {
   comment: {
            keyup: function(textarea){

            }
        },

    },
    // modules
    modules: {
        index: {
            index:function(i){
                
            },

        },

    }
};

$(document).ready(function() {
    Core123F.ready();
    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';
        // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var pagewrapper = '#page-content';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%'; // the menu inside the slide menu itself
    var slidewidth = '80%';
    var menuneg = '-100%';
    var slideneg = '-80%';
     $('#slide-nav.navbar .container').append($('<div id="navbar-height-col"></div>'));
    $(window).on("resize", function () {
        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
        }
    });

    $("#slide-nav").on("click", toggler, function (e) {

        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $('#navbar-height-col').stop().animate({
            left: selected ? slideneg : '0px'
        });

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });


        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');


        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');


    });
});