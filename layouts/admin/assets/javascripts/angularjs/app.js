/**
 * Created by Sandeep on 01/06/14.
 */

angular.module('cinema',['ui.router','ngResource','cinema.controllers','cinema.services']);

angular.module('news',['ui.router','ngResource','news.controllers','news.services']);

angular.module('cinema').config(["$locationProvider", function($locationProvider) {
  $locationProvider.html5Mode(true);
}]);