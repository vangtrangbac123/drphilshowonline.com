/**
 * Created by Sandeep on 01/06/14.
 */

angular.module('cinema.services',[]).factory('Cinema',function($resource){
    return $resource('/ajax?method=:mt',{mt:'@_mt'},{
        update: {
            method: 'PUT'
        }
    });
}).service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
});
//News
angular.module('news.services',[]).factory('News',function($resource){
    return $resource('/ajax?method=:mt',{mt:'@_mt'},{
        update: {
            method: 'PUT'
        }
    });
}).service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
});