<?php

class My_Auth {

    const CACHEKEY = 'admin.auth.%s';
    const COOKIE = 'auth';
    const EXPIRE = 172800; // 2 days

    public static function getCacheKey($uniqid) {
        return sprintf(self::CACHEKEY, $uniqid);
    }

    public static function remember($user) {
        $uniqid = uniqid();
        $key = self::getCacheKey($uniqid);
        $memcache = My_Memcache::getInstance();
        $memcache->set($key, $user, self::EXPIRE);
        @setcookie(self::COOKIE, $uniqid, time() + self::EXPIRE, '/');
    }

    public static function checkAuthCookie() {
        if (!isset($_COOKIE[self::COOKIE])) return false;
        $uniqid = $_COOKIE[self::COOKIE];
        $key = self::getCacheKey($uniqid);
        $memcache = My_Memcache::getInstance();
        $user = $memcache->get($key);
        if ($user) {
            $auth = Zend_Auth::getInstance();
            $auth->getStorage()->write($user);
            $memcache->set($key, $user, self::EXPIRE);
            @setcookie(self::COOKIE, $uniqid, time() + self::EXPIRE, '/');
            return true;
        }
        return false;
    }

}