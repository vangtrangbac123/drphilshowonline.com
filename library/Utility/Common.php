<?php

class Utility_Common
{
	public static function getListActive(){
		return array('Deactivate', 'Active');
	}

	public static function getListStatus(){
		return array(1 => 'Coming soon', 2 => 'Showing');
	}

	public static function getListTypeFilm(){
		return array(0 => 'Loại khác', 1 => 'Khuyến mại', 2 => 'Boom tấn');
	}

	public static function getCateNews(){
		return array(1 => 'Góc điện ảnh', 2 => 'Chỉ có tại 123phim.vn', 3 => 'Movie Guides', 4 => 'Khuyến mãi', 5 => 'Rạp', 6 => 'Giới thiệu');
	}

	public static function getListTypeAdvertise(){
		return array(1 => 'Left', 2 => 'Content', 3 => 'Feedback');
	}

	public static function getListBookingStatus(){
		return array(-1 => "Don't support", 0 => 'Booking', 1 => 'Booking & Payment');
	}

	public static function cienmaBHD(){
		return array(21 => 'BHD 3.2', 28 => 'BHD ICON68', 23 => 'Cinebox - Hòa Bình');
	}

	public static function getStatusNotify($code){
	    $status[0] ='Chưa gửi thành công';
	    $status[1] = 'Gửi thành công';

	    return isset($status[$code]) ? $status[$code] : null;
	}

	public static function statusBooking(){
		return array(0 => "Đơn hàng mới",
					 1 => 'Đang nhập thông tin khách hàng',
					 2 => 'Đang nhập thông tin thanh toán',
					 3 => 'Đang xử lý thanh toán',
					 4 => 'Đặt vé thành công',
					 5 => 'Đơn hàng bị hủy',
					 6 => 'Đơn hàng đang Refund');
	}
	public static function getDevice($deviceId){
		$list =array(0  => 'Web',
			         1  => 'iOS',
			         2  => 'Android',
			         10 => 'Mdot',
			         11 => 'Web',
			         20 => 'Zalo App',
			         21 => 'Momo App',
			         22 => 'Momo App'
			         );
		return isset($list["$deviceId"]) ? $list["$deviceId"] : "Undefine ($deviceId)";
	}

	public static function number_format_vn($num) {
	   return number_format($num, 0, ',', '.');
    }
    public static function getStatus($status_id){
		$status = self::statusBooking();
		return isset($status["$status_id"]) ? $status["$status_id"] : null;
	}
	public static function getColor($status_id){

		// MOMO
		$status['0'] = '#009245';
		$status['1'] = 'green';
		$status['2'] = '#0071bc';
		$status['3'] = '#ed1e79';
		$status['4'] = 'orange';
		$status['5'] = 'black';
		$status['6'] = '#005599';


		return isset($status["$status_id"]) ? $status["$status_id"] : null;
	}

	public static function getPaymentErrorMessage($errorCode) {
		$errors = array();

		$errors['1'] = 'Thanh toán thành công';
		$errors['-22'] = 'Bấm hủy tại trang thanh toán';
		$errors['-24'] = 'Timeout hoặc tắt tại 123P (8 phut)';
		$errors['-25'] = 'Nhập thông tin thanh toán nhưng chưa confirm OTP ';
		$errors['-21'] = 'User bam huy tai trang thanh toan 123P';
		$errors['-23'] = 'User bam huy tai trang thanh toan 123P';
		$errors['0'] = 'Tạo đơn hàng';
		$errors['-100'] = 'Đơn hàng bị hủy';
		$errors['-10'] = 'Đơn hàng bị hủy';
		$errors['5000'] = 'Hệ thống bận';

		$errors['6000'] = 'Xác thực nguồn gọi thất bại';
		$errors['6100'] = 'Tham số truyền vào không đúng định dạng yêu cầu';
		$errors['6200'] = 'Vi phạm Business rule giữa Merchant & 123Pay';
		$errors['6201'] = 'Tài khoản 123Pay đã bị khóa';
		$errors['6211'] = 'Vượt quá giới hạn thanh toán của tài khoản / ngày';
		$errors['6212'] = 'Vượt quá giới hạn thanh toán / giao dịch';

		$errors['7200'] = 'Thông tin thanh toán không hợp lệ';
		$errors['7201'] = 'Không đủ tiền trong tài khoản thanh toán';
		$errors['7202'] = 'Không đảm bảo số dư tối thiểu trong tài khoản thanh toán';
		$errors['7203'] = 'Giới hạn tại bank: Tổng số tiền / ngày';
		$errors['7204'] = 'Giới hạn tại bank: Tổng số giao dịch / ngày';
		$errors['7205'] = 'Giới hạn tại bank: Giá trị / giao dịch';

		$errors['7210'] = 'Khách hàng hủy hoặc không nhập thông tin thanh toán';
		$errors['7211'] = 'Chưa đăng ký dịch vụ thanh toán trực tuyến';
		$errors['7212'] = 'Dịch vụ thanh toán trực tuyến của tài khoản đang tạm khóa';
		$errors['7213'] = 'Tài khoản thanh toán bị khóa';

		$errors['7220'] = 'Khách hàng không nhập OTP';
		$errors['7221'] = 'Nhập thông tin thẻ/tài khoản quá 3 lần';
		$errors['7222'] = 'Invalid OTP';
		$errors['7223'] = 'OTP Expire';
		$errors['7224'] = 'Nhập thông tin OTP quá 3 lần';

		$errors['7231'] = 'Sai tên chủ thẻ';
		$errors['7232'] = 'Card không hợp lệ, không tìm thấy khách hàng / tài khoản';
		$errors['7233'] = 'Expired Card';
		$errors['7234'] = 'Lost Card';
		$errors['7235'] = 'Stolen Card';
		$errors['7236'] = 'Card is marked deleted';

		$errors['7241'] = 'Credit Card - Card Security Code verification failed';
		$errors['7242'] = 'Credit Card - Address Verification Failed';
		$errors['7243'] = 'Credit Card - Address Verification and Card Security Code Failed';
		$errors['7244'] = 'Credit Card - Card did not pass all risk checks';
		$errors['7245'] = 'Credit Card - Bank Declined Transaction';
		$errors['7246'] = 'Credit Card - Account has stop/hold (hold money,...)';
		$errors['7247'] = 'Credit Card - Account closed';
		$errors['7248'] = 'Credit Card - Frozen Account';

		$errors['7252'] = 'Cardholder is not enrolled in Authentication Scheme';
		$errors['7253'] = 'Transaction Aborted/Cancled';
		$errors['7254'] = '3D Secure Authentication failed';
		$errors['7255'] = 'Pickup card';

		$errors['7299'] = 'Thanh toán thất bại';
		$errors['7300'] = 'Lỗi giao tiếp hệ thống ngân hàng';

		// MOMO
		$errors['1006'] = 'Hệ thống đang nâng câp';
		$errors['1001'] = 'Khách hàng không đủ tiền thanh toán';
		$errors['1007'] = 'Khách hàng hủy giao dịch';
		$errors['1008'] = 'Vượt quá giới hạn thanh toán / giao dịch';

		return isset($errors["$errorCode"]) ? $errors["$errorCode"] : null;
	}
}
